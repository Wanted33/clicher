# README #

This project use CocoaPods and you must initialize the dependencies before you open the project. If you don't know how to use CocoaPods you can look a short guide behind :-).

*Note : To use all the functions of the application, you must compile directly on a device and not on a simulator*

### CocoaPods & project installation ###

1. Install cocoapods on the mac with the command: sudo gem install cocoapods
1. Once cocoapods is installed, you must go to the project folder on the command line.
1. Once at the root of the project, execute the command: pod install. Wait for the end of the command (this may take a few minutes).
1. Once all the dependencies are downloaded, open the .xworkspace file with Xcode instead of the .xcodeproj file
1. You must be able to compile without problems