//
//  AppDelegate.swift
//  clicher
//
//  Created by Quentin Rubini on 01/05/2017.
//  Copyright © 2017 Quentin Rubini. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //Show Launch screen during 3 seconds
        Thread.sleep(forTimeInterval: 3.0)
        
        customizeNavigationAndTabBarColor()
        
        if GlobalConstants.isFirstLaunch {
            showTutorialView()
        }
        
        return true
    }
    
    func customizeNavigationAndTabBarColor() {
        UINavigationBar.appearance().barStyle = UIBarStyle.black
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor.applicationPrimaryColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        
        //CUSTOMIZE GENERAL TAB BAR OPTIONS
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().tintColor = UIColor.white
        UITabBar.appearance().barTintColor = UIColor.applicationPrimaryColor()
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.tabBarItemSelectedColor()], for: UIControlState.selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.tabBarItemNormalColor()], for: UIControlState())
    }
    
    func showTutorialView() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tutorialVC = storyboard.instantiateViewController(withIdentifier: "TutorialView") as! TutorialViewController
        self.window?.rootViewController = tutorialVC
    }
    
    func showMainView() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC = storyboard.instantiateViewController(withIdentifier: "RootView") as! UITabBarController
        self.window?.rootViewController = rootVC
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

