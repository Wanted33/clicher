//
//  PhotoPickerViewController.swift
//  clicher
//
//  Created by Quentin Rubini on 01/05/2017.
//  Copyright © 2017 Quentin Rubini. All rights reserved.
//

import UIKit
import AVFoundation

class PhotoPickerViewController: UIViewController, AVCapturePhotoCaptureDelegate {

    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var buttonTakePhoto: UIButton!
    @IBOutlet var viewNoAuthorization: UIView!
    
    var captureSession = AVCaptureSession()
    var sessionOutput = AVCapturePhotoOutput()
    var previewLayer = AVCaptureVideoPreviewLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //Init viewNoAuthorization view
        self.viewNoAuthorization.frame = self.view.frame
        self.viewNoAuthorization.backgroundColor = UIColor.applicationTableViewBackgroundColor()
        self.viewNoAuthorization.isHidden = true
        
        buttonTakePhoto.layer.cornerRadius = buttonTakePhoto.frame.width/2
        buttonTakePhoto.layer.borderColor = UIColor.white.cgColor
        buttonTakePhoto.layer.borderWidth = 2
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) == AVAuthorizationStatus.authorized {
            let deviceDiscoverySession = AVCaptureDeviceDiscoverySession(deviceTypes: [AVCaptureDeviceType.builtInDualCamera, AVCaptureDeviceType.builtInTelephotoCamera,AVCaptureDeviceType.builtInWideAngleCamera], mediaType: AVMediaTypeVideo, position: AVCaptureDevicePosition.front)
            for device in (deviceDiscoverySession?.devices)! {
                if(device.position == AVCaptureDevicePosition.front){
                    do{
                        let input = try AVCaptureDeviceInput(device: device)
                        if(captureSession.canAddInput(input)){
                            captureSession.addInput(input);
                            
                            if(captureSession.canAddOutput(sessionOutput)){
                                captureSession.addOutput(sessionOutput);
                                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession);
                                previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
                                previewLayer.connection.videoOrientation = AVCaptureVideoOrientation.portrait;
                                previewView.layer.addSublayer(previewLayer);
                                captureSession.startRunning()
                                
                                self.buttonTakePhoto.isHidden = false
                                self.viewNoAuthorization.isHidden = true
                                self.previewView.isHidden = false
                            }
                        }
                    }
                    catch{
                        print("exception!");
                    }
                }
            }
        } else {
            self.buttonTakePhoto.isHidden = true
            self.viewNoAuthorization.isHidden = false
            self.previewView.isHidden = true
            self.view.insertSubview(viewNoAuthorization, at: 0)
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        previewLayer.frame = previewView.bounds
    }
    
    // MARK: - Photo delegate
    func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?, previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        if let error = error {
            print("error occure : \(error.localizedDescription)")
        }
        
        if  let sampleBuffer = photoSampleBuffer,
            let previewBuffer = previewPhotoSampleBuffer,
            let dataImage =  AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer:  sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
            
            let dataProvider = CGDataProvider(data: dataImage as CFData)
            let cgImageRef: CGImage! = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: .defaultIntent)
            let image = UIImage(cgImage: cgImageRef, scale: 1.0, orientation: UIImageOrientation.right)
            PhotoAlbumManager.sharedInstance.save(image: image, result: { (success, error) in
                if (success) {
                    MessageManager.sharedInstance.showMessageUnderStatusBar("Your photo has been saved !")
                } else {
                    MessageManager.sharedInstance.showMessageUnderStatusBar("An error has occurred")
                }
            })
        } else {
            print("Some error here")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func buttonTakePhotoPressed(sender: UIButton) {
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [
            kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
            kCVPixelBufferWidthKey as String: 160,
            kCVPixelBufferHeightKey as String: 160
        ]
        settings.previewPhotoFormat = previewFormat
        sessionOutput.capturePhoto(with: settings, delegate: self)
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
