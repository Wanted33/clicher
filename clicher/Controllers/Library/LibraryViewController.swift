//
//  LibraryViewController.swift
//  clicher
//
//  Created by Quentin Rubini on 01/05/2017.
//  Copyright © 2017 Quentin Rubini. All rights reserved.
//

import UIKit
import Photos

class LibraryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonPhotoPicker: UIButton!
    @IBOutlet var viewNoAuthorization: UIView!
    
    var allAlbums: [Album] = [Album]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.estimatedRowHeight = 66
        
        //Get all albums
        getAllAlbums()
        PHPhotoLibrary.shared().register(self) //register observer to refresh table when there is changes in albums
        
        //Init viewNoAuthorization view
        self.viewNoAuthorization.frame = self.view.frame
        self.viewNoAuthorization.backgroundColor = UIColor.applicationTableViewBackgroundColor()
        self.viewNoAuthorization.isHidden = true
        
        buttonPhotoPicker.layer.cornerRadius = buttonPhotoPicker.frame.width/2
    }
    
    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getAllAlbums() {
        PhotoAlbumManager.sharedInstance.getAllAlbumsWithPhotos {
            result in
            self.allAlbums = result
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if PHPhotoLibrary.authorizationStatus() != PHAuthorizationStatus.authorized {
            self.viewNoAuthorization.isHidden = false
            self.tableView.isHidden = true
            self.view.insertSubview(viewNoAuthorization, at: 0)
            PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) -> Void in
                ()
            })
        }else {
            self.viewNoAuthorization.isHidden = true
            self.tableView.isHidden = false
            self.view.insertSubview(self.tableView, at: 0)
        }
        
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
     
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allAlbums.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! AlbumTableViewCell
     
        // Configure the cell...
        let album = allAlbums[indexPath.row]
        
        //Customize title
        cell.labelTitle!.text = album.title
        
        //Customize subtitle
        let plurialSubtitle = (album.images.count > 1) ? "images" : "image";
        cell.labelSubtitle!.text = "\(album.images.count) \(plurialSubtitle)"
        
        //Customize image
        if (album.images.count > 0) {
            cell.imageViewAlbum.image = album.images[0]
        } else {
            cell.imageViewAlbum.image = UIImage(named: "noImage")
        }
        
        return cell
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "detailSegue") {
            let indexPath = self.tableView.indexPathForSelectedRow
            let libraryDetailVC = segue.destination as! LibraryDetailsCollectionViewController
            libraryDetailVC.currentAlbum = self.allAlbums[indexPath!.row]
        }
    }

}

// MARK: PHPhotoLibraryChangeObserver
extension LibraryViewController: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        // Change notifications may be made on a background queue. Re-dispatch to the
        // main queue before acting on the change as we'll be updating the UI.
        DispatchQueue.main.sync {
            getAllAlbums()
        }
    }
}
