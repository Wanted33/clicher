//
//  TutorialViewController.swift
//  clicher
//
//  Created by Quentin Rubini on 01/05/2017.
//  Copyright © 2017 Quentin Rubini. All rights reserved.
//

import UIKit

class TutorialViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    var tutorialViewControllers = [PageItemViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        // Do any additional setup after loading the view.
        //Init page controller
        initTutorialViews()
        self.setViewControllers([tutorialViewControllers[0] as! TutorialFirstViewController], direction: .forward, animated: true, completion: nil)
        self.delegate = self
        self.dataSource = self
        
        //Customize background
        self.view.backgroundColor = UIColor.applicationPrimaryColor()
        
        //Customize page control
        let proxy: UIPageControl = UIPageControl.appearance()
        proxy.pageIndicatorTintColor = UIColor.applicationDarkPrimaryColor()
        proxy.currentPageIndicatorTintColor = UIColor.white
        proxy.backgroundColor = UIColor.clear
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initTutorialViews() {
        let firstViewController = self.storyboard?.instantiateViewController(withIdentifier: "FirstTutorialView") as! TutorialFirstViewController
        firstViewController.itemIndex = 0
        firstViewController.tutorialVC = self
        tutorialViewControllers.append(firstViewController)
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "SecondTutorialView") as! TutorialSecondViewController
        secondViewController.itemIndex = 1
        secondViewController.tutorialVC = self
        tutorialViewControllers.append(secondViewController)
        
        let thirdViewController = self.storyboard?.instantiateViewController(withIdentifier: "ThirdTutorialView") as! TutorialThirdViewController
        thirdViewController.itemIndex = 2
        thirdViewController.tutorialVC = self
        tutorialViewControllers.append(thirdViewController)
    }
    

    // MARK: - Page View Controller
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let controller = viewController as? PageItemViewController {
            if controller.itemIndex > 0 {
                return tutorialViewControllers[controller.itemIndex - 1]
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let controller = viewController as? PageItemViewController {
            if controller.itemIndex < tutorialViewControllers.count - 1 {
                return tutorialViewControllers[controller.itemIndex + 1]
            }
        }
        return nil
    }
    
    // MARK: - Page Control
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return self.tutorialViewControllers.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        if let pageItemVC = self.viewControllers?[0] as? PageItemViewController {
            return pageItemVC.itemIndex
        }
        return 0
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
