//
//  TutorialThirdViewController.swift
//  clicher
//
//  Created by Quentin Rubini on 01/05/2017.
//  Copyright © 2017 Quentin Rubini. All rights reserved.
//

import UIKit

class TutorialThirdViewController: PageItemViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var buttonStart: PrimaryButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.labelTitle.text = "Take unlimited selfies"
        self.labelDescription.text = "Are you a snapchat addict?  So don't wait anymore and take hundreds of selfies"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func startButtonPressed(_ sender: Any) {
        GlobalConstants.isFirstLaunch = false
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showMainView()
    }
}
