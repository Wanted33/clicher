//
//  ApplicationStyleExtension.swift
//  clicher
//
//  Created by Quentin Rubini on 01/05/2017.
//  Copyright © 2017 Quentin Rubini. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    class func applicationPrimaryColor() -> UIColor {
        return UIColor(red:0.16, green:0.73, blue:0.61, alpha:1.00)
    }
    
    class func applicationDarkPrimaryColor() -> UIColor {
        return UIColor(red:0.14, green:0.62, blue:0.52, alpha:1.00)
    }
    
    class func applicationSecondaryColor() -> UIColor {
        return UIColor(red:0.22, green:0.79, blue:0.45, alpha:1.00)
    }
    
    class func applicationTableViewBackgroundColor() -> UIColor {
        return UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.00)
    }
    
    class func applicationGrayBorderColor() -> UIColor {
        return UIColor(red:0.71, green:0.71, blue:0.71, alpha:1.00)
    }
    
    class func tabBarItemNormalColor() -> UIColor {
        return UIColor(red:0.85, green:0.85, blue:0.85, alpha:1.00)
    }
    
    class func tabBarItemSelectedColor() -> UIColor {
        return UIColor.white
    }
}
