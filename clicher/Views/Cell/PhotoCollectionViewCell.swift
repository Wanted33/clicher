//
//  PhotoCollectionViewCell.swift
//  clicher
//
//  Created by Quentin Rubini on 01/05/2017.
//  Copyright © 2017 Quentin Rubini. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
}
