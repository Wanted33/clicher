//
//  MessageManager.swift
//  clicher
//
//  Created by Quentin Rubini on 01/05/2017.
//  Copyright © 2017 Quentin Rubini. All rights reserved.
//

import UIKit
import SwiftMessages

class MessageManager: NSObject {
    static let sharedInstance = MessageManager()
    
    func showMessageUnderStatusBar(_ text: String) {
        let status = MessageView.viewFromNib(layout: .StatusLine)
        status.backgroundView.backgroundColor = UIColor.applicationPrimaryColor()
        status.bodyLabel?.textColor = UIColor.white
        status.configureContent(body: text)
        
        var statusConfig = SwiftMessages.defaultConfig
        statusConfig.presentationContext = .window(windowLevel: UIWindowLevelNormal)
        statusConfig.preferredStatusBarStyle = .lightContent
        
        SwiftMessages.show(config: statusConfig, view: status)
    }
}
