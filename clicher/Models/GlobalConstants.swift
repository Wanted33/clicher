//
//  GlobalConstants.swift
//  clicher
//
//  Created by Quentin Rubini on 01/05/2017.
//  Copyright © 2017 Quentin Rubini. All rights reserved.
//

import Foundation
struct GlobalConstants {
    
    static var appAlbumName : String = "Clicher"
    
    static var isFirstLaunch: Bool {
        get {
            if let firstLaunch = UserDefaults.standard.object(forKey: "isFirstLaunch") as? Bool {
                return firstLaunch
            } else {
                return true
            }
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: "isFirstLaunch")
        }
    }
}
